# frozen_string_literal: true

Rails.application.routes.draw do
  resources :general_reports, except: %i[edit update]
  resources :reports, only: %i[index show]
  root 'general_reports#index'
end
