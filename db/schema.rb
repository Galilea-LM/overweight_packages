# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_01_03_013446) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "general_reports", force: :cascade do |t|
    t.text "data"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "reports", force: :cascade do |t|
    t.string "tracking_number"
    t.string "carrier"
    t.float "volumetric_weight"
    t.float "weight_kg"
    t.integer "total_weight"
    t.integer "overweight"
    t.float "weight_fedex"
    t.bigint "general_report_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["general_report_id"], name: "index_reports_on_general_report_id"
  end

end
