# frozen_string_literal: true

class CreateReports < ActiveRecord::Migration[6.0]
  def change
    create_table :reports do |t|
      t.string :tracking_number
      t.string :carrier
      t.float :volumetric_weight
      t.float :weight_kg
      t.integer :total_weight
      t.integer :overweight
      t.float :weight_fedex
      t.belongs_to :general_report

      t.timestamps
    end
  end
end
