# frozen_string_literal: true

class CreateGeneralReports < ActiveRecord::Migration[6.0]
  def change
    create_table :general_reports do |t|
      t.text :data

      t.timestamps
    end
  end
end
