# frozen_string_literal: true

require 'test_helper'

class ReportTest < ActiveSupport::TestCase
  test 'is valid' do
    report = Report.new valid_params

    assert report.valid?
  end

  test 'is invalid' do
    report = Report.new invalid_params

    refute report.valid?
    assert_equal 2, report.errors.count
  end

  test 'is invalid wihtout unique traking number' do
    saved_report = reports(:one)
    report = Report.new valid_params.merge(tracking_number: saved_report.tracking_number)

    refute report.valid?
    assert_equal 1, report.errors.count
  end

  def valid_params
    {
      tracking_number: '449044304137821',
      carrier: 'FEDEX',
      volumetric_weight: 3.1,
      weight_kg: 2.5,
      total_weight: 4,
      overweight: 1,
      weight_fedex: 4,
      general_report_id: 1
    }
  end

  def invalid_params
    {}
  end
end
