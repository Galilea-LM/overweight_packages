# frozen_string_literal: true

require 'test_helper'

class GeneralReportTest < ActiveSupport::TestCase
  test 'is valid' do
    general_report = GeneralReport.new valid_params

    assert general_report.valid?
  end

  test 'is invalid' do
    general_report = GeneralReport.new invalid_params

    refute general_report.valid?
    assert_equal 1, general_report.errors.count
  end

  def valid_params
    { data: [
      {
        "tracking_number": '149331877648230',
        "carrier": 'FEDEX',
        "parcel": {
          "length": 29.7,
          "width": 5,
          "height": 21,
          "weight": 2.0,
          "distance_unit": 'CM',
          "mass_unit": 'KG'
        }
      }
    ] }
  end

  def invalid_params
    {}
  end
end
