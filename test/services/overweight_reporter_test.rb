# frozen_string_literal: true

require 'test_helper'
require 'json'

class OverweightReporterTest < ActionDispatch::IntegrationTest
  setup do
    @reporter = OverweightReporter.new(general_reports(:one))
  end

  test 'should return a hash with params to create new report' do
    result = @reporter.report_params(JSON.parse(general_reports(:one).data).first.with_indifferent_access)
    data = { tracking_number: '568838414941', carrier: 'FEDEX', volumetric_weight: 2.3273, weight_kg: 1.5, general_report_id: 980_190_962 }

    assert_equal(data, result)
  end

  test 'should return a measure on metrical system' do
    data = { distance_unit: 'IN', width: '3.2', height: '5.4', length: '6' }
    result = @reporter.weigth_volume(data)

    assert_equal(0.3398021591040001, result)
  end

  test 'shoul return a measure on kilograms' do
    data = { mass_unit: 'LB', weight: '3.2' }
    result = @reporter.weight_kg(data)

    assert_equal(1.4514944, result)
  end
end
