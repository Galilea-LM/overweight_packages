# frozen_string_literal: true

require 'test_helper'

class ReportsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @report = reports(:one)
  end

  test 'get index' do
    get reports_path
    assert_response :success
  end

  test 'show report' do
    get report_path(@report)
    assert_response :success
  end
end
