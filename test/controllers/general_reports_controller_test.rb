# frozen_string_literal: true

require 'test_helper'

# rubocop:disable  Metrics/ClassLength
class GeneralReportsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @general_report = general_reports(:one)
  end

  test 'create individuals reports' do
    assert_difference 'Report.count', valid_params.count do
      post general_reports_path, params: { general_report: { data: valid_params.to_json } }
    end

    assert_redirected_to general_report_path(GeneralReport.last)
  end

  test 'get index' do
    get general_reports_path
    assert_response :success
  end

  test 'get new' do
    get new_general_report_path
    assert_response :success
  end

  test 'show general_report' do
    get general_report_path(@general_report)
    assert_response :success
  end

  test 'destroy general_report' do
    assert_difference 'GeneralReport.count', -1 do
      delete general_report_path(@general_report)
    end

    assert_redirected_to general_reports_path
  end
  # rubocop:disable  Metrics/MethodLength
  def valid_params
    [
      {
        "tracking_number": '149331877648230',
        "carrier": 'FEDEX',
        "parcel": {
          "length": 29.7,
          "width": 5,
          "height": 21,
          "weight": 2.0,
          "distance_unit": 'CM',
          "mass_unit": 'KG'
        }
      },
      {
        "tracking_number": '449044304137821',
        "carrier": 'FEDEX',
        "parcel": {
          "length": 30,
          "width": 25,
          "height": 10,
          "weight": 1,
          "distance_unit": 'CM',
          "mass_unit": 'KG'
        }
      },
      {
        "tracking_number": '920241085725456',
        "carrier": 'FEDEX',
        "parcel": {
          "length": 20,
          "width": 20,
          "height": 20,
          "weight": 3.5,
          "distance_unit": 'CM',
          "mass_unit": 'KG'
        }
      },
      {
        "tracking_number": '568838414941',
        "carrier": 'FEDEX',
        "parcel": {
          "length": 37,
          "width": 37,
          "height": 8.5,
          "weight": 1.5,
          "distance_unit": 'CM',
          "mass_unit": 'KG'
        }
      },
      {
        "tracking_number": '039813852990618',
        "carrier": 'FEDEX',
        "parcel": {
          "length": 8,
          "width": 8,
          "height": 19,
          "weight": 0.5,
          "distance_unit": 'CM',
          "mass_unit": 'KG'
        }
      },
      {
        "tracking_number": '231300687629630',
        "carrier": 'FEDEX',
        "parcel": {
          "length": 20,
          "width": 15,
          "height": 15,
          "weight": 3.2,
          "distance_unit": 'CM',
          "mass_unit": 'KG'
        }
      },
      {
        "tracking_number": '122816215025810',
        "carrier": 'FEDEX',
        "parcel": {
          "length": 28,
          "width": 22,
          "height": 2,
          "weight": 0.1,
          "distance_unit": 'CM',
          "mass_unit": 'KG'
        }
      }
    ]
  end
  # rubocop:enable  Metrics/MethodLength
end
# rubocop:enable  Metrics/ClassLength
