# frozen_string_literal: true

require 'fedex'
require 'json'

class OverweightReporter < ApplicationService
  attr_reader :general_report

  def initialize(general_report)
    @general_report = general_report
    @fedex = Fedex::Shipment.new(key: Rails.application.credentials.dig(:fedex, :key),
                                 password: Rails.application.credentials.dig(:fedex, :password),
                                 account_number: Rails.application.credentials.dig(:fedex, :account_number),
                                 meter: Rails.application.credentials.dig(:fedex, :meter),
                                 mode: Rails.application.credentials.dig(:fedex, :mode))
  end

  def call
    JSON.parse(@general_report.data).each do |report|
      @general_report.reports.create(report_params(report.with_indifferent_access).merge(fedex_data(report.with_indifferent_access)))
    end
  end

  def report_params(report)
    {
      tracking_number: report[:tracking_number],
      carrier: report[:carrier],
      volumetric_weight: weigth_volume(report[:parcel]),
      weight_kg: weight_kg(report[:parcel]),
      general_report_id: @general_report.id
    }
  end

  # rubocop:disable  Metrics/AbcSize
  def weigth_volume(measure)
    if (measure[:distance_unit] == 'CM') || (measure[:units] == 'CM')
      ((measure[:width].to_f * measure[:height].to_f * measure[:length].to_f) / 5000)
    else
      (((measure[:width].to_f * 2.54) * (measure[:height].to_f * 2.54) * (measure[:length].to_f * 2.54)) / 5000)
    end
  end
  # rubocop:enable  Metrics/AbcSize

  def weight_kg(measure)
    return measure[:weight].to_f if measure[:mass_unit] == 'KG'

    (measure[:weight].to_f * 0.453592)
  end

  def weight_total(measure)
    return weight_kg(measure).ceil if weight_kg(measure) > weigth_volume(measure)

    weigth_volume(measure).ceil
  end

  def fedex_data(report)
    tracking_info = @fedex.track(tracking_number: report[:tracking_number]).first

    weight_total_fedex = weight_total({ mass_unit: tracking_info.details[:package_weight][:units],
                                        weight: tracking_info.details[:package_weight][:value] }.merge(tracking_info.details[:package_dimensions]))

    weight_total_platform = weight_total(report[:parcel])

    { total_weight: weight_total_platform, weight_fedex: weight_total_fedex, overweight: overweight_fedex(weight_total_fedex, weight_total_platform) }
  end

  def overweight_fedex(weight_fedex, weight_platform)
    (weight_fedex - weight_platform).ceil
  end
end
