# frozen_string_literal: true

class GeneralReportsController < ApplicationController
  before_action :find_resource, only: %i[show destroy]

  def index
    @general_reports = GeneralReport.all
  end

  def show; end

  def new
    @general_report = GeneralReport.new
  end

  def create
    @general_report = GeneralReport.new(general_report_params)

    respond_to do |format|
      if @general_report.save
        OverweightReporter.call(@general_report)
        format.html { redirect_to @general_report, notice: t('flash.create.general_report.notice') }
        format.json { render :show, status: :created, location: @general_report }
      else
        format.html { render :new, alert: t('flash.create.general_report.alert') }
        format.json { render json: @general_report.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    respond_to do |format|
      if @general_report.destroy
        format.html { redirect_to general_reports_url, notice: t('flash.destroy.general_report.notice') }
        format.json { head :no_content }
      else
        format.html { redirect_to @general_report, alert: t('flash.destroy.general_report.alert') }
        format.json { render json: @general_report.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def find_resource
    @general_report = GeneralReport.find(params[:id])
  end

  def general_report_params
    { data: params[:general_report][:data] }
  end
end
