# frozen_string_literal: true

json.extract! report, :id, :tracking_number, :carrier, :weight_fedex, :total_weight, :overweight, :created_at, :updated_at
json.url report_url(report, format: :json)
