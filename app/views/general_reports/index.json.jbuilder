# frozen_string_literal: true

json.array! @general_reports, partial: 'general_reports/general_report', as: :general_report
