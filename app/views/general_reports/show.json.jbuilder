# frozen_string_literal: true

json.partial! 'general_reports/general_report', general_report: @general_report
