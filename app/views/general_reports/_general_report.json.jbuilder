# frozen_string_literal: true

json.extract! general_report, :id, :data, :created_at, :updated_at
json.url general_report_url(general_report, format: :json)
