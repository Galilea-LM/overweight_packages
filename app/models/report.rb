# frozen_string_literal: true

class Report < ApplicationRecord
  validates :carrier, presence: true
  validates :tracking_number, presence: true, uniqueness: true
end
