# frozen_string_literal: true

class GeneralReport < ApplicationRecord
  has_many :reports, dependent: :destroy
  validates :data, presence: true
end
